//
//  BookObject.swift
//  BookFinder
//
//  Created by Chris on 11/22/16.
//  Copyright © 2016 Chris. All rights reserved.
//

import UIKit

class BookObject: NSObject {

    var title:String!
    var author:String!
    var publisher:String!
    var imagelink:String!
    var cover_image:UIImage!
    var selflink:String!
    var descriptString:String!
    var isbn13:String!
    
    var apple_price : String!
    var apple_link : String!
    
    var google_price : String!
    var google_retail_price : String!
    var google_link : String!
    
}

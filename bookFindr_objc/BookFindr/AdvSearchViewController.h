//
//  AdvSearchViewController.h
//  BookFindr
//
//  Created by Venkata S
//  Copyright © 2016 AppBros. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvSearchViewController : UIViewController

@property (strong, nonatomic) UIAlertController *info;
@property (strong, nonatomic) NSString *searchStringavc;
@property (strong, nonatomic) NSString *urlStringavc;
@property (weak, nonatomic) IBOutlet UIButton *advSubmit;

@end
